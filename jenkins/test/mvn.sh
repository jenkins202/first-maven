#!/bin/bash

echo "***************************"
echo "** Testing the code ***********"
echo "***************************"
WORKSPACE=/home/sanket/Sumit/Docker-Jenkins/Docker-Jenkins/jenkins_home/workspace/maven-job

docker run --rm  -v  $WORKSPACE/:/app -v /root/.m2/:/root/.m2/ -w /app maven:3-alpine "$@"
